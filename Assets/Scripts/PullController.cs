﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class PullController : MonoBehaviour
{
    [SerializeField] private float maxPullTime = .25f;
    [Serializable]
    public class PullEndedEvent : UnityEvent<bool, float> { }
    [SerializeField] private PullEndedEvent PullEnded;
    
    [Serializable]
    public class PullUpdateEvent : UnityEvent<float> { }
    [SerializeField] private PullUpdateEvent PullUpdate;
    
    [SerializeField] private UnityEvent PullStarted;
    private float currentPullTime = 0f;
    private bool reachedMaxTime = false;

    private void Update()
    {
        var fire = Input.GetButton("Fire1");
        PullUpdate?.Invoke(currentPullTime/maxPullTime);
        if (fire)
        {
            if (reachedMaxTime) return;
            if (currentPullTime == 0f) PullStarted?.Invoke();
            if (currentPullTime >= maxPullTime)
            {
                currentPullTime = 0f;
                reachedMaxTime = true;
                PullEnded?.Invoke(false, -1f);
                return;
            }

            currentPullTime += Time.deltaTime;
        }
        else
        {
            if (currentPullTime > 0f) PullEnded?.Invoke(true, currentPullTime / maxPullTime);
            reachedMaxTime = false;
            currentPullTime = 0f;
        }
    }
}