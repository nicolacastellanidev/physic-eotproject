﻿using System;
using UnityEngine;


public class AutoDestroy : MonoBehaviour
{
    public float lifeTime = 1f;

    private void Start()
    {
        Invoke(nameof(Destroy), lifeTime);
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }
}