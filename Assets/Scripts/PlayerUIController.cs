﻿using UnityEngine;

public class PlayerUIController : MonoBehaviour
{
    [SerializeField] private Transform timeBarFg;
    private float _targetTimebarFg = 0f;
    
    public void SetTimeBarScale(float amount)
    {
        _targetTimebarFg = amount;
    }

    private void Awake()
    {
        var localScaleTimeBar = timeBarFg.localScale;
        localScaleTimeBar.x = 0f;
        timeBarFg.localScale = localScaleTimeBar;
    }

    private void LateUpdate()
    {
        var localScaleTimeBar = timeBarFg.localScale;
        localScaleTimeBar.x = Mathf.Lerp(localScaleTimeBar.x, _targetTimebarFg, .1f);
        timeBarFg.localScale = localScaleTimeBar;
    }
}
