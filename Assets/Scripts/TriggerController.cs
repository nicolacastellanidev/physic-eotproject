﻿using UnityEngine;

public class TriggerController : MonoBehaviour
{
    public float impulseForce = 10f;
    public GameObject particlesPrefab;
    private void OnCollisionEnter(Collision other)
    {
        var rb = other.gameObject.GetComponent<Rigidbody>();
        if (!rb) return;
        var contactPoint = other.GetContact(0);
        var dir = -contactPoint.normal;
        dir = dir.normalized;
        rb.AddForce(dir * impulseForce, ForceMode.Impulse);
        var go = Instantiate(particlesPrefab, contactPoint.point, Quaternion.LookRotation(dir));
        go.AddComponent<AutoDestroy>();
    }
}