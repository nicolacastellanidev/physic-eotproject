﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class EffectsHandler : MonoBehaviour
{
    private Camera _camera;
    private float _targetTimeScale = 1f;
    private float _targetFov = 0f;
    private float _originalFov = 0f;
    private float _targetChromatic = 0f;
    private float _targetVignette = 0f;

    private ChromaticAberration m_Chromatic;
    private Vignette m_Vignette;

    PostProcessVolume m_cameraVolume;

    private void Awake()
    {
        _camera = Camera.main;
        m_cameraVolume = _camera.GetComponent<PostProcessVolume>();
        _originalFov = _camera.fieldOfView;
        _targetFov = _originalFov;
    }

    private void Start()
    {
        m_Chromatic = ScriptableObject.CreateInstance<ChromaticAberration>();
        m_Vignette = ScriptableObject.CreateInstance<Vignette>();
        m_Chromatic.enabled.Override(true);
        m_Vignette.enabled.Override(true);
        m_Chromatic.intensity.Override(0f);
        m_Vignette.intensity.Override(0f);

        m_cameraVolume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, m_Chromatic);
        m_cameraVolume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, m_Vignette);
        
        GameManager.onGameEnded += DestoryMe;
    }

    private void DestoryMe()
    {
        Time.timeScale = 0f;
        _targetTimeScale = 0f;
        Destroy(this);
    }

    public void StartEffects()
    {
        _targetTimeScale = 0.1f;
        _targetFov = 90f;
        _targetChromatic = 1f;
        _targetVignette = 0.6f;
    }

    public void StopEffects()
    {
        _targetTimeScale = 1f;
        _targetFov = _originalFov;
        _targetChromatic = 0;
        _targetVignette = 0;
    }

    private void LateUpdate()
    {
        if (Time.timeScale != _targetTimeScale)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, _targetTimeScale, 0.05f);
        }

        if (_camera.fieldOfView != _targetFov)
        {
            _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, _targetFov, 0.05f);
        }

        if (m_Chromatic.intensity.value != _targetChromatic)
        {
            m_Chromatic.intensity.value = Mathf.Lerp(m_Chromatic.intensity.value, _targetChromatic, 0.05f);
        }

        if (m_Vignette.intensity.value != _targetVignette)
        {
            m_Vignette.intensity.value = Mathf.Lerp(m_Vignette.intensity.value, _targetVignette, 0.05f);
        }
    }

    /*void OnDestroy()
    {
        RuntimeUtilities.DestroyVolume(m_cameraVolume, true, true);
    }*/
}