﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private Transform sourceOfForce;
    [SerializeField] private float maxForce = 200f;
    [SerializeField] private float torqueForce = 10f;
    [SerializeField] private float jumpForce = 20f;

    [SerializeField] private float yForce = .1f;
    
    private Rigidbody _mRigidbody;
    
    private float pulledAmount = 0f;

    private void Awake()
    {
        _mRigidbody = GetComponentInChildren<Rigidbody>();
    }

    public void AddForce(bool pullSuccessfully, float forceMultiplier)
    {
        if (!pullSuccessfully) return;
        _mRigidbody.velocity = Vector3.zero;
        var nextForce = maxForce * forceMultiplier;
        var dir = transform.position - sourceOfForce.position;
        dir = dir.normalized;
        dir.y = yForce;
        _mRigidbody.AddForce(dir * nextForce, ForceMode.Impulse);
    }
    

    public void Reset()
    {
        transform.position = new Vector3(0, 2, 0);
        _mRigidbody.angularVelocity = Vector3.zero;
        _mRigidbody.angularDrag = 0f;
        _mRigidbody.velocity = Vector3.zero;
    }
}