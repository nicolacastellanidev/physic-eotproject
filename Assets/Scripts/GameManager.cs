﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public static UnityAction onGameEnded;
    
    [SerializeField] private GameObject winPanel;
    [SerializeField] private Text totalTimeText;
    [SerializeField] private Text timerText;
    private float timer;

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        } else if (Instance != this)
        {
            Destroy(this);
        }
        Instance.winPanel.SetActive(false);
    }

    private void Update()
    {
        timer += Time.deltaTime;
        timerText.text = Instance.GetFormattedTimer();
    }

    public static void Win()
    {
        onGameEnded?.Invoke();
        Instance.totalTimeText.text = Instance.GetFormattedTimer();
        Instance.winPanel.SetActive(true);
    }
    
    public void Restart()
    {
        Time.timeScale = 0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    public void Proceed()
    {
        Time.timeScale = 0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private string GetFormattedTimer()
    {
        var minutes = Mathf.FloorToInt(Instance.timer / 60F);
        var seconds = Mathf.FloorToInt(Instance.timer - minutes * 60);
        var milliseconds = Mathf.FloorToInt(Instance.timer * 1000f)%1000f;
        return string.Format("{0:0}:{1:00}.{2:000}", minutes, seconds, milliseconds);
    }
}
