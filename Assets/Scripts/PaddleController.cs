﻿using UnityEngine;

public class PaddleController : MonoBehaviour
{
    public Transform center;
    public float speed = 1f;
    private Vector2 screenCenter;

    private float direction = 1f;

    private void Start()
    {
        var cam = Camera.main;
        Cursor.lockState = CursorLockMode.Confined;
        screenCenter = new Vector2(cam.pixelWidth / 2f, cam.pixelHeight / 2f);
    }

    private void Update()
    {
        transform.RotateAround(center.position, Vector3.up, direction * 180f * Time.deltaTime * speed);
        if (Input.GetButtonUp("Fire1"))
        {
            direction *= -1f;
        }
    }

    /*void Update()
    {
        var xDelta = (screenCenter.x - Input.mousePosition.x) / screenCenter.x;
        
        xDelta = Mathf.Clamp(xDelta, -1f, 1f);

        xDelta *= 3f;

        var xPos = -5f * Mathf.Sin(xDelta);
        var yPos = -5f * Mathf.Cos(xDelta);

        transform.position = new Vector3(
            xPos + center.position.x,
            transform.position.y,
            yPos + center.position.z
            );
        
        transform.LookAt(center.position);
    }*/
}