﻿using UnityEngine;

public class PlayerFollowController : MonoBehaviour
{
    private Transform mPlayer;
    [SerializeField] private Vector3 offset;
    [SerializeField] private GameObject indicator;
    private void Awake()
    {
        var player = GameObject.FindWithTag("Player");
        if (!player)
        {
            Debug.LogError("No player found");
            Destroy(this);
        }
        mPlayer = player.transform;
        ToggleIndicator(false);
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, mPlayer.transform.position + offset, 10f);
    }

    public void ToggleIndicator(bool active)
    {
        if (!indicator) return;
        indicator.SetActive(active);
    }
}
