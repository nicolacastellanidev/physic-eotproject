﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject follow = null;

    public float force = 10f;
    public float torqueForce = 10f;

    public bool active = true;
    
    private Rigidbody _mRigidBody;

    private void Awake()
    {
        _mRigidBody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (!active) return;

        var dir = follow.transform.position - transform.position;

        dir = dir.normalized;
        
        _mRigidBody.AddForce(dir * force);
    }
}
