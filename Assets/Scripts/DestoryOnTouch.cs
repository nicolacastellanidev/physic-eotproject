﻿using UnityEngine;

public class DestoryOnTouch : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Ball>().Reset();
            return;
        }
        Destroy(other.gameObject);
    }
}
